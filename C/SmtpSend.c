/*

SmtpSend, utility to send email from the command line.
Copyright (C) 2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/


#include <PodNet/PodNet.h>
#include <PodNet/CString/CString.h>
#include <PodNet/CString/CHeapString.h>
#include <PodNet/CNetworking/CSmtp.h>



typedef struct __STATE
{
	HANDLE 		hSmtp;
	LPSTR 		lpszTo;
	LPSTR 		lpszCc;
	LPSTR 		lpszFrom;
	LPSTR 		lpszTitle;
	LPSTR 		lpszBody;
	LPSTR 		lpszServer;
	SMTP_PROTOCOL	spType;
} STATE, *LPSTATE;




_Kills_Process_
INTERNAL_OPERATION
KILL
__PrintHelp(
	VOID
){
	PrintFormat("PodNet -- SmtpSend\n");
	PrintFormat("------------------\n");
	PrintFormat("\n");
	PrintFormat("A Simple Utility To Send Email\n");
	PrintFormat("Licensed Under The AGPLv3 License\n");
	PrintFormat("\nUsage:\n");
	PrintFormat("\t-h, --help\t- Print Help\n");
	PrintFormat("\t-l, --license\t- Print License Info\n");
	PrintFormat("\t-t, --to\t- To Address\n");
	PrintFormat("\t-f, --from\t- From Address\n");
	PrintFormat("\t-t, --title\t- Set Email Title\n");
	PrintFormat("\t-b, --body\t- Body Of Email\n");
	PrintFormat("\t-s, --server\t- SMTP Server\n");
	PrintFormat("\t-P, --protocol\t- Protocol, 1 for plain, 2 for SSL, 3 for TLS\n");
	PrintFormat("\n");
	PostQuitMessage(0);
}




_Kills_Process_
INTERNAL_OPERATION
KILL
__PrintLicense(
	VOID
){
	PrintFormat("SmtpSend, utility to send email from the command line.\n");
	PrintFormat("Copyright (C) 2019 Zach Podbielniak\n");
	PrintFormat("\n");
	PrintFormat("This program is free software: you can redistribute it and/or modify\n");
	PrintFormat("it under the terms of the GNU Affero General Public License as\n");
	PrintFormat("published by the Free Software Foundation, either version 3 of the\n");
	PrintFormat("License, or (at your option) any later version.\n");
	PrintFormat("\n");
	PrintFormat("This program is distributed in the hope that it will be useful,\n");
	PrintFormat("but WITHOUT ANY WARRANTY; without even the implied warranty of\n");
	PrintFormat("MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n");
	PrintFormat("GNU Affero General Public License for more details.\n");
	PrintFormat("\n");
	PrintFormat("You should have received a copy of the GNU Affero General Public License\n");
	PrintFormat("along with this program.  If not, see <https://www.gnu.org/licenses/>.\n");
	PostQuitMessage(0);
}



_Success_(return != FALSE, _Non_Locking_)
INTERNAL_OPERATION
BOOL
__ParseArgs(
	_In_ 		LPSTATE 	lpState,
	_In_ 		LONG 		lArgCount,
	_In_ 		DLPSTR 		dlpszArgValues
){
	LPSTR lpszKey, lpszValue;
	UARCHLONG ualIndex;

	for (
		ualIndex = 1;
		ualIndex < lArgCount;
		ualIndex++
	){
		lpszKey = dlpszArgValues[ualIndex];

		if ('-' == lpszKey[0])
		{
			if (ualIndex != (lArgCount - 1))
			{ lpszValue = dlpszArgValues[ualIndex + 1]; }
			else 
			{ lpszValue = NULLPTR; }

			/* Help */
			if (
				0 == StringCompare("--help", lpszKey) ||
				0 == StringCompare("-h", lpszKey)
			){ __PrintHelp(); }
			/* License */
			if (
				0 == StringCompare("--license", lpszKey) ||
				0 == StringCompare("-l", lpszKey)
			){ __PrintLicense(); }
			/* To */
			else if (
				0 == StringCompare("--to", lpszKey) ||
				0 == StringCompare("-t", lpszKey)
			){ lpState->lpszTo = lpszValue;	}
			/* From */
			else if (
				0 == StringCompare("--from", lpszKey) ||
				0 == StringCompare("-f", lpszKey)
			){ lpState->lpszFrom = lpszValue; }
			/* Cc */
			else if (
				0 == StringCompare("--cc", lpszKey) ||
				0 == StringCompare("-c", lpszKey)
			){ lpState->lpszCc = lpszValue;	}
			/* Title */
			else if (
				0 == StringCompare("--title", lpszKey) ||
				0 == StringCompare("-T", lpszKey)
			){ lpState->lpszTitle = lpszValue; }
			/* Body */
			else if (
				0 == StringCompare("--body", lpszKey) ||
				0 == StringCompare("-b", lpszKey)
			){ lpState->lpszBody = lpszValue; }
			/* Server */
			else if (
				0 == StringCompare("--server", lpszKey) ||
				0 == StringCompare("-s", lpszKey)
			){ lpState->lpszServer = lpszValue; }
			/* Type */
			else if (
				0 == StringCompare("--protocol", lpszKey) ||
				0 == StringCompare("-P", lpszKey)
			){ 
				UARCHLONG ualInput;

				if (NULLPTR == lpszValue)
				{
					WriteFile(GetStandardError(), "Protocol was empty\n", 0);
					PostQuitMessage(1);
				}

				StringScanFormat(
					lpszValue,
					"%lu",
					&ualInput
				);

				switch (ualInput)
				{
					case SMTP_PROTOCOL_PLAIN:
					{
						lpState->spType = SMTP_PROTOCOL_PLAIN;
						break;
					}
					case SMTP_PROTOCOL_SSL:
					{
						lpState->spType = SMTP_PROTOCOL_SSL;
						break;
					}
					case SMTP_PROTOCOL_TLS:
					{
						lpState->spType = SMTP_PROTOCOL_TLS;
						break;
					}
					default: 
					{
						WriteFile(GetStandardError(), "Protocol is undefined\n", 0);
						PostQuitMessage(1);
						break;
					}
				}
			}
		}
	}

	return TRUE;
}




LONG
Main(
	_In_ 		LONG 		lArgCount,
	_In_ 		DLPSTR 		dlpszArgValues
){
	STATE sProgram;
	ZeroMemory(&sProgram, sizeof(sProgram));

	__ParseArgs(&sProgram, lArgCount, dlpszArgValues);

	sProgram.hSmtp = CreateSmtpConnection(
		sProgram.spType,
		sProgram.lpszServer,
		NULLPTR,
		NULLPTR,
		TRUE,
		FALSE
	);

	SendEmail(
		sProgram.hSmtp,
		sProgram.lpszFrom,
		sProgram.lpszTo,
		sProgram.lpszCc,
		sProgram.lpszTitle,
		sProgram.lpszBody
	);

	DestroyObject(sProgram.hSmtp);
	return 0;
}