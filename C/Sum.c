/*

Sum, a tool to sum an arbitrary set of values.
Copyright (C) 2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/


#include <PodNet/Ascii.h>
#include <PodNet/CFile/CFile.h>
#include <PodNet/CContainers/CVector.h>


LONG Main(LONG lArgCount, DLPSTR dlpszParams)
{
	if (2 > lArgCount)
	{ return 1; }

	UARCHLONG ualIndex;
	BOOL bStdIn, bFloat;
	HVECTOR hvValues;


	for(
		ualIndex = 1;
		ualIndex < lArgCount;
		ualIndex++
	){
		if (0 == StringCompare("-i", dlpszParams[ualIndex]))
		{ 
			bStdIn = TRUE; 
			dlpszParams[ualIndex] = NULLPTR;
		}
		else if (0 == StringCompare("-f", dlpszParams[ualIndex]) ||
			 0 == StringCompare("--float", dlpszParams[ualIndex]))
		{ 
			bFloat = TRUE; 
			dlpszParams[ualIndex] = NULLPTR;
		}
	}

	if (TRUE == bFloat)
	{ hvValues = CreateVector(1000, sizeof(DOUBLE), NULL); }
	else
	{ hvValues = CreateVector(1000, sizeof(ULONGLONG), NULL); }
	
	EXIT_IF_UNLIKELY_NULL(hvValues, 1);

	if (TRUE == bStdIn)
	{
		UARCHLONG ualSize;
		LPSTR lpszLine;
	
		ualSize = 4096;
		lpszLine = LocalAllocAndZero(sizeof(CHAR) * 4096);

		while (0 < getline(&lpszLine, &ualSize, stdin))
		{
			if (NULLPTR == lpszLine)
			{ JUMP(__Sum); }

			LPSTR lpszTemp;

			lpszTemp = StringInString(lpszLine, "\n");
			if (NULLPTR != lpszTemp)
			{ *lpszTemp = 0; }

			if (TRUE == bFloat)
			{
				DOUBLE dbX;
				if (0 != StringScanFormat(lpszLine, "%lf", &dbX))
				{ VectorPushBack(hvValues, &dbX, NULL); }
			}
			else
			{
				ULONGLONG ullX;
				if (0 != StringScanFormat(lpszLine, "%llu", &ullX))
				{ VectorPushBack(hvValues, &ullX, NULL); }
			}
		}
	}
	else
	{
		for(
			ualIndex = 1;
			ualIndex < lArgCount;
			ualIndex++
		){
			if (NULLPTR != dlpszParams[ualIndex])
			{
				if (TRUE == bFloat)
				{
					DOUBLE dbX;
					if (0 != StringScanFormat(dlpszParams[ualIndex], "%lf", &dbX))
					{ VectorPushBack(hvValues, &dbX, NULL); }
				}
				else
				{
					ULONGLONG ullX;
					if (0 == StringScanFormat(dlpszParams[ualIndex], "%llu", &ullX))
					{ VectorPushBack(hvValues, &ullX, NULL); }
				}
			}
		}
	}
	

	/* Find Sum */
	__Sum:
	if (TRUE == bFloat)
	{
		DOUBLE dbSum;
		
		for (
			ualIndex = 0;
			ualIndex < VectorSize(hvValues);
			ualIndex++
		){ dbSum += *(LPDOUBLE)VectorAt(hvValues, ualIndex); }

		PrintFormat("%f\n", dbSum);
	}
	else
	{
		ULONGLONG ullSum;

		for (
			ualIndex = 0;
			ualIndex < VectorSize(hvValues);
			ualIndex++
		){ ullSum += *(LPULONGLONG)VectorAt(hvValues, ualIndex); }
	}

	return 0;
}