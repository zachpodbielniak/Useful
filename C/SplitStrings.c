/*

SplitStrings, a tool to split input based on delimeters
Copyright (C) 2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/


#include <PodNet/CFile/CFile.h>
#include <PodNet/CString/CString.h>
#include <PodNet/CContainers/CVector.h>


#define BUFFER_SIZE		4096




typedef struct __ARGS 
{
	BOOL 		bStdIn;
	BOOL 		bStdOut;
	BOOL  		bFileIn;
	BOOL 		bFileOut;
	BOOL 		bAppend;

	LPSTR 		lpszDelim;
	LPSTR 		lpszSentence;
	LPSTR 		lpszFileIn;
	LPSTR 		lpszFileOut;
	
	UARCHLONG 	ualCount;

	HVECTOR 	hvSentences;
	HANDLE 		hFileIn;
	HANDLE 		hFileOut;
} ARGS, *LPARGS;




INTERNAL_OPERATION
VOID
__PrintHelp(
	VOID
){
	PrintFormat("SplitStrings - By Zach Podbielniak\n");
	PrintFormat("Released Under The GPLv3 License\n\n");
	PrintFormat("About:\n");
	PrintFormat("\tSplitStrings is a simple utility that will split one or more strings \n");
	PrintFormat("\tthat are input by a specifed delimiter, or set of delimiters, and \n");
	PrintFormat("\tprint each token on its own line.\n\n");
	PrintFormat("Format:\n\tsplitstrings [ARGS] \"[DELIMITER(S)]\" \"[SENTENCE]\" \"[...]\"\n");
	PrintFormat("\nArgs:\n");
	PrintFormat("\t-h \t  -> Print this help info\n");
	PrintFormat("\t-i [FILE] -> Specify input file, if just -i, reads from StdIn.\n");
	PrintFormat("\t-c \t  -> Count, that is, the number of lines to read from input.\n");
	PrintFormat("\t-o [FILE] -> Output to file, if just -o, outputs to StdOut.\n");
	PrintFormat("\n");

	return;
}




INTERNAL_OPERATION
BOOL
__GetArgs(
	_In_ 		LPARGS 		lpArgs,
	_In_ 		LONG 		lArgCount,
	_In_		TLPSTR		tlpszArgValues
){
	BOOL bSetDelim;
	UARCHLONG ualIndex;

	for (
		ualIndex = 1;
		ualIndex < lArgCount;
		ualIndex++
	){
		if (0 == StringCompare("-h", (*tlpszArgValues)[ualIndex]) ||
			0 == StringCompare("--help", (*tlpszArgValues)[ualIndex]))
		{
			__PrintHelp();
			return FALSE;
		}
		/* Input, either StdIn, or a File */
		else if (0 == StringCompare("-i", (*tlpszArgValues)[ualIndex]) ||
			 0 == StringCompare("--input", (*tlpszArgValues)[ualIndex]))
		{ 
			if (ualIndex == (lArgCount - 1))
			{ 
				lpArgs->bStdIn = TRUE; 
				break;
			} 
			
			/* Specifying a file */
			if ('-' != (*tlpszArgValues)[ualIndex + 1][0])
			{
				lpArgs->lpszFileIn = (*tlpszArgValues)[ualIndex + 1];
				lpArgs->bFileIn = TRUE;
				ualIndex++;
			}
			else
			{ lpArgs->bStdIn = TRUE; }
		}
		/* Output file, either stdout or a File */
		else if (0 == StringCompare("-o", (*tlpszArgValues)[ualIndex]) ||
			 0 == StringCompare("--output", (*tlpszArgValues)[ualIndex]))
		{
			if (ualIndex == (lArgCount - 1))
			{
				lpArgs->bStdOut = TRUE;
				break;
			}

			/* Specifying a file */
			if ('-' != (*tlpszArgValues)[ualIndex + 1][0])
			{
				lpArgs->lpszFileOut = (*tlpszArgValues)[ualIndex + 1];
				lpArgs->bFileOut = TRUE;
				ualIndex++;
			}
			else
			{ lpArgs->bStdOut = TRUE; }
		
		}
		/* Append File */
		else if (0 == StringCompare("-a", (*tlpszArgValues)[ualIndex]) ||
			 0 == StringCompare("--append", (*tlpszArgValues)[ualIndex]))
		{ lpArgs->bAppend = TRUE; }
		/* The count, necessary if using StdIn from PIPE */
		else if (0 == StringCompare("-c", (*tlpszArgValues)[ualIndex]))
		{ 
			if (ualIndex == (lArgCount - 1))
			{ break ;}

			if ('-' == (*tlpszArgValues)[ualIndex + 1][0])
			{
				lpArgs->ualCount = 0;
				break;
			}
			else if (0 == StringScanFormat((*tlpszArgValues)[ualIndex + 1], "%llu", &(lpArgs->ualCount)))
			{ 
				lpArgs->ualCount = 0; 
				break; 
			}

			ualIndex++;
		}
		/* First non switched input is the delimeter */
		else if (FALSE == bSetDelim)
		{ 
			lpArgs->lpszDelim = (*tlpszArgValues)[ualIndex]; 
			bSetDelim = TRUE;
		}
		/* Parse the rest as a sentence to split */
		else
		{ 
			if (TRUE == lpArgs->bStdIn)
			{ break; }
			else
			{ VectorPushBack(lpArgs->hvSentences, &((*tlpszArgValues)[ualIndex]), NULL); }
		}
	}

	return TRUE;
}

INTERNAL_OPERATION
BOOL
__SpecifyInputOutput(
	_In_ 		LPARGS 		lpArgs
){
	EXIT_IF_UNLIKELY_NULL(lpArgs, FALSE);

	/* Specify Output File */
	if (TRUE == lpArgs->bFileOut && TRUE == lpArgs->bAppend)
	{
		lpArgs->hFileOut = OpenFile((LPCSTR)lpArgs->lpszFileOut, FILE_PERMISSION_APPEND | FILE_PERMISSION_READ, NULL);
		EXIT_IF_UNLIKELY_NULL(lpArgs->hFileOut, FALSE);
	}
	else if (TRUE == lpArgs->bFileOut)
	{
		lpArgs->hFileOut = OpenFile((LPCSTR)lpArgs->lpszFileOut, FILE_PERMISSION_CREATE | FILE_PERMISSION_WRITE, NULL);
		EXIT_IF_UNLIKELY_NULL(lpArgs->hFileOut, FALSE);
	}
	else
	{
		lpArgs->hFileOut = GetStandardOut();
		EXIT_IF_UNLIKELY_NULL(lpArgs->hFileOut, FALSE);
	}

	/* Specify Input File */
	if (TRUE == lpArgs->bFileIn)
	{
		lpArgs->hFileIn = OpenFile((LPCSTR)lpArgs->lpszFileIn, FILE_PERMISSION_READ, NULL);
		EXIT_IF_UNLIKELY_NULL(lpArgs->hFileIn, FALSE);
	}
	else if (TRUE == lpArgs->bStdIn)
	{
		lpArgs->hFileIn = GetStandardIn();
		EXIT_IF_UNLIKELY_NULL(lpArgs->hFileIn, FALSE);
	}

	return TRUE;
}


INTERNAL_OPERATION
VOID
__FileIn(
	_In_ 		LPARGS 		lpArgs
){
	DLPSTR dlpszSplit;
	LPSTR lpszSentence;
	UARCHLONG ualSize;
	UARCHLONG ualIndex, ualJndex;

	lpszSentence = LocalAllocAndZero(sizeof(CHAR) * BUFFER_SIZE);
               
	ualSize = BUFFER_SIZE - 1;
	ualIndex = 0;

	if (lpArgs->ualCount == 0)
	{ lpArgs->ualCount = MAX_ULONG; }

	while(ualIndex < lpArgs->ualCount && (0 < ReadLineFromFile(lpArgs->hFileIn, &lpszSentence, ualSize, NULL)))
	{
		CHAR szBuffer[BUFFER_SIZE];
		ZeroMemory(szBuffer, sizeof(szBuffer));

	      	StringSplit(lpszSentence, lpArgs->lpszDelim, &dlpszSplit);
		
		for (
			ualJndex = 0;
			FALSE == STRING_SPLIT_NULL_TERMINATOR(dlpszSplit[ualJndex]);
			ualJndex++
		){ 
			if (FALSE != STRING_SPLIT_NULL_TERMINATOR(dlpszSplit[ualJndex + 1]))
			{ 
				StringPrintFormatSafe(szBuffer, BUFFER_SIZE - 1, "%s", dlpszSplit[ualJndex]); 
				WriteFile(lpArgs->hFileOut, (LPCSTR)szBuffer, NULL);
			}
			else
			{ 
				StringPrintFormatSafe(szBuffer, BUFFER_SIZE - 1, "%s\n", dlpszSplit[ualJndex]); 
				WriteFile(lpArgs->hFileOut, (LPCSTR)szBuffer, NULL);
			}
		}
		
		DestroySplitString(dlpszSplit);
		ZeroMemory(lpszSentence, 4096);
		ualIndex++;
	}
	
	FreeMemory(lpszSentence);
	return;
}




INTERNAL_OPERATION
VOID
__VectorIn(
	_In_ 		LPARGS 		lpArgs
){
	LPSTR lpszSentence;
	DLPSTR dlpszSplit;
	UARCHLONG ualIndex, ualJndex;

	for (
		ualIndex = 0;
		ualIndex < VectorSize(lpArgs->hvSentences);
		ualIndex++
	){
		CHAR szBuffer[BUFFER_SIZE];
		ZeroMemory(szBuffer, sizeof(szBuffer));

		lpszSentence = *(DLPSTR)VectorAt(lpArgs->hvSentences, ualIndex);
		StringSplit(lpszSentence, lpArgs->lpszDelim, &dlpszSplit);

		for (
			ualJndex = 0;
			FALSE == STRING_SPLIT_NULL_TERMINATOR(dlpszSplit[ualJndex]);
			ualJndex++
		){
			StringPrintFormatSafe(szBuffer, BUFFER_SIZE - 1, "%s\n", dlpszSplit[ualJndex]); 
			WriteFile(lpArgs->hFileOut, (LPCSTR)szBuffer, NULL);
		}

		DestroySplitString(dlpszSplit);
	}

	return;
}




_Success_(return == 0, _Non_Locking_)
LONG 
Main(
	_In_ 		LONG 		lArgCount, 
	_In_Z_ 		DLPSTR 		dlpszArgValues
){
        if (1 >= lArgCount)
        { return 1; }
	LPARGS lpArgs;

	BOOL bSetDelim;
	LPSTR lpszSentence;
	DLPSTR dlpszSplit;
	UARCHLONG ualIndex, ualJndex;

	lpArgs = GlobalAllocAndZero(sizeof(ARGS));
	EXIT_IF_UNLIKELY_NULL(lpArgs, 10);

	bSetDelim = FALSE;
	lpArgs->bStdIn = FALSE;
	lpArgs->bStdOut = FALSE;
	lpArgs->bFileIn = FALSE;
	lpArgs->bFileOut = FALSE;
	lpArgs->ualCount = 0;
	lpArgs->hvSentences = CreateVector(0xA, sizeof(LPSTR), NULL);
	lpArgs->hFileIn = NULL_OBJECT;
	lpArgs->hFileOut = NULL_OBJECT;


	/* Get Arguments */
	if (FALSE == __GetArgs(lpArgs, lArgCount, &dlpszArgValues))
	{ JUMP(__SafeExit); }

	/* Specify IO */
	if (FALSE == __SpecifyInputOutput(lpArgs))
	{ JUMP(__SafeExit); }

        /* Which one are we doing? */
	if (TRUE == lpArgs->bStdIn || TRUE == lpArgs->bFileIn)
	{ __FileIn(lpArgs); }
	else
	{ __VectorIn(lpArgs); }


	__SafeExit:
	if (GetStandardIn() != lpArgs->hFileIn && NULL_OBJECT != lpArgs->hFileIn)
	{ DestroyObject(lpArgs->hFileIn); }

	if (GetStandardOut() != lpArgs->hFileOut && NULL_OBJECT != lpArgs->hFileOut)
	{ DestroyObject(lpArgs->hFileOut); }

	if (NULLPTR != lpArgs->hvSentences)
	{ DestroyVector(lpArgs->hvSentences); }

	FreeMemory(lpArgs);

	return 0;
}