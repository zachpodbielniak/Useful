/*

XUtil, an X11 utility for scripting.
Copyright (C) 2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/


#include <PodNet/PodNet.h>
#include <PodNet/CSystem/CSystem.h>
#include <X11/Xlib.h>
#include <X11/extensions/XTest.h>


typedef struct __PARAMS 
{
	LONG 		lArgCount;
	DLPSTR 		dlpszArgValues;

	LONG 		lWarpX;
	LONG 		lWarpY;

	LONG 		lButton;


	BOOL 		bWarp;
	BOOL 		bClick;
} PARAMS, *LPPARAMS;




_Kills_Process_
INTERNAL_OPERATION
KILL 
__PrintHelp(
	VOID
){
	PrintFormat("PodNet -- XUtil\n");
	PrintFormat("------------------\n");
	PrintFormat("\n");
	PrintFormat("A Simple Utility For X11 Macros\n");
	PrintFormat("Licensed Under The AGPLv3 License\n");
	PrintFormat("\nUsage:\n");
	PrintFormat("\t-h, --help\t- Print Help\n");
	PrintFormat("\t-l, --license\t- Print License Info\n");
	PrintFormat("\t-w, --warp\t- Warp to x:y\n");
	PrintFormat("\t-c, --click\t- Mouse Click LEFT | RIGHT | MIDDLE");
	PrintFormat("\n");
	PostQuitMessage(0);
}




_Kills_Process_
INTERNAL_OPERATION
KILL 
__PrintLicense(
	VOID
){
	PrintFormat("XUtil, an X11 utility for scripting.\n");
	PrintFormat("Copyright (C) 2019 Zach Podbielniak\n");
	PrintFormat("\n");
	PrintFormat("This program is free software: you can redistribute it and/or modify\n");
	PrintFormat("it under the terms of the GNU Affero General Public License as\n");
	PrintFormat("published by the Free Software Foundation, either version 3 of the\n");
	PrintFormat("License, or (at your option) any later version.\n");
	PrintFormat("\n");
	PrintFormat("This program is distributed in the hope that it will be useful,\n");
	PrintFormat("but WITHOUT ANY WARRANTY; without even the implied warranty of\n");
	PrintFormat("MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n");
	PrintFormat("GNU Affero General Public License for more details.\n");
	PrintFormat("\n");
	PrintFormat("You should have received a copy of the GNU Affero General Public License\n");
	PrintFormat("along with this program.  If not, see <https://www.gnu.org/licenses/>.\n");
	PostQuitMessage(0);
}




_Success_(return != FALSE, _Non_Locking_)
INTERNAL_OPERATION
BOOL 
__ParseArgs(
	_In_ 		LPPARAMS	lppState
){
	LPSTR lpszKey, lpszValue;
	UARCHLONG ualIndex;

	for (
		ualIndex = 1;
		ualIndex < lppState->lArgCount;
		ualIndex++
	){
		lpszKey = lppState->dlpszArgValues[ualIndex];

		if ('-' == lpszKey[0])
		{
			if (ualIndex != (lppState->lArgCount - 1))
			{ lpszValue = lppState->dlpszArgValues[ualIndex + 1]; }
			else 
			{ lpszValue = NULLPTR; }


			/* Help */
			if (
				0 == StringCompare("--help", lpszKey) ||
				0 == StringCompare("-h", lpszKey)
			){ __PrintHelp(); }

			else if (
				0 == StringCompare("--license", lpszKey) ||
				0 == StringCompare("-l", lpszKey)
			){ __PrintLicense(); }

			else if (
				0 == StringCompare("--warp", lpszKey) ||
				0 == StringCompare("-w", lpszKey)
			){
				if (NULLPTR == lpszValue)
				{ PostQuitMessage(1); }

				StringScanFormat(
					lpszValue,
					"%d:%d",
					&(lppState->lWarpX),
					&(lppState->lWarpY)
				);

				lppState->bWarp = TRUE;
			}

			else if (
				0 == StringCompare("--click", lpszKey) ||
				0 == StringCompare("-c", lpszKey)
			){
				if (NULLPTR == lpszValue)
				{ PostQuitMessage(1); }

				LONG lButton;

				if (0 == StringCompare("LEFT", lpszValue))
				{ lButton = 1; }
				else if (0 == StringCompare("MIDDLE", lpszValue))
				{ lButton = 2; }
				else if (0 == StringCompare("RIGHT", lpszValue))
				{ lButton = 3; }
				else
				{ lButton = 0; }
				
				if (0 != lButton)
				{ 
					lppState->bClick = TRUE;
					lppState->lButton = lButton;
				}
			}
		}
	}

	return TRUE;
}




_Success_(return != FALSE, _Non_Locking_)
INTERNAL_OPERATION
BOOL 
__Warp(
	_In_ 		LONG 		lX,
	_In_ 		LONG 		lY
){
	Display *lpdMonitor;
	
	lpdMonitor = XOpenDisplay(0);
	if (NULLPTR == lpdMonitor)
	{ PostQuitMessage(1); }

	XWarpPointer(
		lpdMonitor,
		None,
		RootWindow(
			lpdMonitor, 
			DefaultScreen(lpdMonitor)
		),
		0,
		0,
		0,
		0,
		lX,
		lY
	);

	XSetInputFocus(
		lpdMonitor,
		PointerRoot,
		RevertToPointerRoot,
		CurrentTime
	);

	XCloseDisplay(lpdMonitor);
	return TRUE;
}




_Success_(return != FALSE, _Non_Locking_)
INTERNAL_OPERATION
BOOL 
__Click(
	_In_ 		LONG 		lButton,
	_In_ 		ULONG 		ulDelay
){
	Display *lpdMonitor;

	lpdMonitor = XOpenDisplay(0);
	if (NULLPTR == lpdMonitor)
	{ PostQuitMessage(1); }

	XTestFakeButtonEvent(lpdMonitor, lButton, TRUE, CurrentTime);
	Sleep(ulDelay);
	XTestFakeButtonEvent(lpdMonitor, lButton, FALSE, CurrentTime);
	
	XFlush(lpdMonitor);
	XCloseDisplay(lpdMonitor);
	return TRUE;
}



_Success_(return == 0, _Non_Locking_)
LONG 
Main(
	_In_		LONG 		lArgCount,
	_In_Z_ 		DLPSTR 		dlpszArgValues
){
	PARAMS pState;

	ZeroMemory(&pState, sizeof(PARAMS));

	pState.lArgCount = lArgCount;
	pState.dlpszArgValues = dlpszArgValues; 

	__ParseArgs(&pState);


	if (FALSE != pState.bWarp)
	{ __Warp(pState.lWarpX, pState.lWarpY); }

	if (FALSE != pState.bClick)
	{ __Click(pState.lButton, 10); }

	return 0;
}