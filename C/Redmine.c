#include <PodNet/PodNet.h>
#include <PodNet/CNetworking/CHttpsRequest.h>
#include <jansson.h>

#define API_KEY		""

typedef json_t		*LPJSON;
typedef json_error_t	JSON_ERROR, *LPJSON_ERROR;


LONG
Main(
	_In_ 		LONG 			lArgCount,
	_In_Z_ 		DLPSTR 			dlpszArgValues
){
	UNREFERENCED_PARAMETER(lArgCount);
	UNREFERENCED_PARAMETER(dlpszArgValues);

	HANDLE hRequest;
	CSTRING csBuffer[8192];
	LPJSON lpjPayload, lpjIssue, lpjData;
	JSON_ERROR jeStatus;
	LPSTR lpszIndex;
	
	ULONG ulId;
	CSTRING csSubject[512];
	CSTRING csDescription[512];
	CSTRING csStatus[512];

	ZeroMemory(csBuffer, sizeof(csBuffer));
	ZeroMemory(csSubject, sizeof(csSubject));
	ZeroMemory(csDescription, sizeof(csDescription));
	ZeroMemory(csStatus, sizeof(csStatus));

	hRequest = CreateHttpsRequest(
		HTTP_REQUEST_GET,
		HTTP_VERSION_1_1,
		"issues.podbielniak.com",
		443,
		"/issues/15.json?key=" API_KEY,
		HTTP_CONNECTION_CLOSE,
		"PodMine/0.1",
		HTTP_ACCEPT_JSON,
		FALSE,
		TRUE
	);

	ExecuteHttpsRequest(
		hRequest,
		csBuffer,
		sizeof(csBuffer) - 1,
		NULLPTR
	);

	lpjPayload = json_loads(csBuffer, 0, &jeStatus);
	lpjIssue = json_object_get(lpjPayload, "issue");

	lpjData = json_object_get(lpjIssue, "id");
	ulId = json_number_value(lpjData);

	lpjData = json_object_get(lpjIssue, "subject");
	lpszIndex = (LPSTR)json_string_value(lpjData);
	StringCopySafe(csSubject, lpszIndex, sizeof(csSubject) - 1);

	lpjData = json_object_get(lpjIssue, "description");
	lpszIndex = (LPSTR)json_string_value(lpjData);
	StringCopySafe(csDescription, lpszIndex, sizeof(csDescription) - 1);

	PrintFormat("ID: %lu\nSubject: %s\nDescription: %s\n", ulId, csSubject, csDescription);

	json_decref(lpjData);
	json_decref(lpjPayload);
	DestroyObject(hRequest);
	return 0;
}