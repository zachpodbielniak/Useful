/*

Weather, a tool to get the weather report, from C.
Copyright (C) 2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/


#include <PodNet/PodNet.h>
#include <PodNet/CNetworking/CHttpsRequest.h>
#include <jansson.h>

#define DARKSKY_API_KEY			"API_KEY_HERE"
#define LATITUDE			"41.7669"
#define LONGITUDE			"-83.6212"


LONG Main(LONG lArgCount, DLPSTR dlpszArgValues)
{
	UNREFERENCED_PARAMETER(lArgCount);
	UNREFERENCED_PARAMETER(dlpszArgValues);

	HANDLE hRequest;
	CSTRING csData[1638400];
	CSTRING csSummary[8192];
	json_t *jPayload;
	json_t *jCurrent;
	json_t *jData;
	json_error_t error;
	DOUBLE dbTemperature, dbHumidity, dbCloudCover;
	DOUBLE dbWindSpeed, dbWindGust, dbWindBearing;
	DOUBLE dbUvIndex, dbVisibility;
	DOUBLE dbPrecipProb, dbPrecipIntensity;
	LPSTR lpszSummary;

	ZeroMemory(csData, sizeof(csData));
	ZeroMemory(csSummary, sizeof(csSummary));

	hRequest = CreateHttpsRequestEx(
		HTTP_REQUEST_GET,
		HTTP_VERSION_1_1,
		"api.darksky.net",
		443,
		"/forecast/" DARKSKY_API_KEY "/" LATITUDE "," LONGITUDE,
		HTTP_CONNECTION_CLOSE,
		"PodNet",
		HTTP_ACCEPT_JSON,
		NULLPTR,
		0,
		NULLPTR,
		0,
		FALSE,
		TRUE
	);

	ExecuteHttpsRequest(hRequest, csData, sizeof(csData) - 1, FALSE);

	/* Pull Info From 'Currently' */
	jPayload = json_loads(csData, 0, &error);
	jCurrent = json_object_get(jPayload, "currently");
	
	jData = json_object_get(jCurrent, "summary");
	lpszSummary = (LPSTR)json_string_value(jData);
	StringCopySafe(
		csSummary,
		lpszSummary,
		sizeof(csSummary) - 1
	);

	jData = json_object_get(jCurrent, "temperature");
	dbTemperature = json_number_value(jData);

	jData = json_object_get(jCurrent, "humidity");
	dbHumidity = json_number_value(jData) * 100.0f;

	jData = json_object_get(jCurrent, "cloudCover");
	dbCloudCover = json_number_value(jData) * 100.0f;

	jData = json_object_get(jCurrent, "windSpeed");
	dbWindSpeed = json_number_value(jData);

	jData = json_object_get(jCurrent, "windGust");
	dbWindGust = json_number_value(jData);
	
	jData = json_object_get(jCurrent, "windBearing");
	dbWindBearing = json_number_value(jData);
	
	jData = json_object_get(jCurrent, "uvIndex");
	dbUvIndex = json_number_value(jData);

	jData = json_object_get(jCurrent, "visibility");
	dbVisibility = json_number_value(jData);

	jData = json_object_get(jCurrent, "precipProbability");
	dbPrecipProb = json_number_value(jData);
	
	jData = json_object_get(jCurrent, "precipIntensity");
	dbPrecipIntensity = json_number_value(jData);
	json_decref(jData);
	







	PrintFormat("Summary:\t\t\t%s\n", csSummary);
	PrintFormat("Temperature:\t\t\t%3.1fF\n", dbTemperature);
	PrintFormat("Humidity:\t\t\t%3.1f%%\n", dbHumidity);
	PrintFormat("Cloud Cover:\t\t\t%3.1f%%\n", dbCloudCover);
	PrintFormat("Chance Of Precipitation:\t%3.1f%%\n", dbPrecipProb);
	PrintFormat("Precipitation Intensity:\t%3.1f\n", dbPrecipIntensity);
	PrintFormat("Wind:\t\t\t\t%3.1f MPH\nGusts:\t\t\t\t%3.1f MPH\nWind Bearing:\t\t\t%3.0f\n", dbWindSpeed, dbWindGust, dbWindBearing);
	PrintFormat("Visibility:\t\t\t%3.1f Miles\n", dbVisibility);
	PrintFormat("UV Index:\t\t\t%3.1f\n", dbUvIndex);

	
	json_decref(jCurrent);
	json_decref(jPayload);
	DestroyObject(hRequest);
	return 0;
}