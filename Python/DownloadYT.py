#!/usr/bin/python3
from pytube import YouTube
import asyncio
import uvloop
import sys


def download_video(url: str) -> bool:
	video: YouTube = YouTube(url)
	video.bypass_age_gate()
	title: str = video.title

	print(f"Downloading {title}")
	stream = video.streams.get_highest_resolution()
	stream.download(output_path="./", filename=f"{title}.mp4")
	print(f"Finished {title}")


def get_urls() -> list[str]:
	if (len(sys.argv) > 1):
		return sys.argv[1:]
	else:
		print("You need to provide URLs!")
		sys.exit(1)


async def main() -> None:
	urls: list[str] = get_urls()
	
	print(urls)

	results: tuple[bool] = await asyncio.gather(
		*[asyncio.to_thread(download_video, url) for url in urls]
	)

	if False in results:
		sys.exit(2)


loop = uvloop.new_event_loop()
asyncio.set_event_loop(loop)
loop.run_until_complete(main())
