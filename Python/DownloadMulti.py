import asyncio
import uvloop
import sys

ydl_args: list[str] = []


async def run_youtube_dl(url: str) -> None:
	cmd: str = "/usr/local/bin/yt-dlp"

	args = ydl_args.copy()
	args.append(url)
	
	proc = await asyncio.create_subprocess_exec(
		cmd,
		*args,
		stdout=asyncio.subprocess.PIPE,
		stdin=asyncio.subprocess.DEVNULL
	)

	print(f"Downloading {url}")
	status_code = await proc.wait()
	print(f"Download of {url} finished with exit code {status_code}")


def set_args_and_get_urls() -> list[str]:
	args: list[str] = sys.argv[1:]
	urls: list[str] = []

	for arg in args:
		if (False == arg.startswith("http")):
			ydl_args.append(arg)
		else:
			urls.append(arg)		
	
	return urls


async def main() -> None:
	coroutines: list = map(run_youtube_dl, set_args_and_get_urls())
	await asyncio.gather(*coroutines)


loop = uvloop.new_event_loop()
asyncio.set_event_loop(loop)
loop.run_until_complete(main())
